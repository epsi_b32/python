
run_all: run_exo

run_exo:
	python3 exo.py

migrate:
	python3 manage.py migrate

install:
	pip install -r ./requirement.txt
