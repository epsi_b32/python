from utils import printedelement
printedelement.page_head("EXERCICES")

printedelement.exercie_head("EXERCICE 1")

def exo1():
    prenom ="toto"
    age = 20
    majeur = True
    sold_baque = 30.55
    amis = ['tinkywinky', 'razmo', 'rapido']
    parent= ("lala","pau")

    print(prenom)
    print(age)
    print(majeur)
    print(sold_baque)
    print(amis)
    print(parent)
exo1()

printedelement.exercie_head("EXERCICE 2")

def exo2():
    nombre = 15
    print("Le nombre est "+str(nombre))
exo2()

printedelement.exercie_head("EXERCICE 3")

def exo3():
    a = 2
    b=6
    c=3
    print(a,'+',b,'+',c)
    print(a,b,c, sep=' + ')

printedelement.exercie_head("EXERCICE 4")

def exo4():
    list1 = range(3)
    list2 = range(5)

    print(list(list2))

printedelement.exercie_head("EXERCICE 5")

def test(var):
    if(isinstance(var,str)):
        return "string"
    elif isinstance(var,int):
        return "integer"
    else:
        return "non définit"

def exo5():
    prenom = "Pierre"
    print("ceci est un ", test(prenom))
    prenom = 0
    print("ceci est un ", test(prenom))


printedelement.exercie_head("EXERCICE 8") ## remplacer un mot dans une phrase
def exo8(repaced_value="ciao"):
    phrase = "bonjour, bonjour et encore bonjour"
    print(phrase.replace("bonjour",repaced_value))
    print(phrase.replace("bonjour",repaced_value,1))
    
exo8()

printedelement.exercie_head("EXERCICE 10")
def exo10(rayon=10):
    from math import pi
    print('l\'aire est de : ',round(4/3*pi*rayon**3,2), "cm²")
    
exo10()

printedelement.exercie_head("EXERCICE 11")
def exo11():
    while True: 
        try:
            nombre = int(input('entrez un nombre : ')) # cette ligne retourne une exeption si ce n'est pas un INT
            break # permet de passer à la suite du code
        except:
            print("choisissez une nombre !")
        
    if nombre>10:
        print(nombre," supérieur à 10")
    else:
        print(nombre," inférieur à 10")
    
# exo11()

printedelement.exercie_head("EXERCICE 12")
def exo12():
    num1,num2 = 5, 15
    tab = list(range(num2+1)[num1:])
    print(tab)
    
exo12()
    
