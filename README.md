# python

##### Table of Contents  
- [Lancer les projets](#lancer-les-projets)  
  - [Les-run](#run-disponibles) 
- [Cours](#cours)  
  - [Les-Bases](#les-bases) 

## Lancer les projets

initialiser :

```bash
python3 -m venv venv
make install
make run_XXX 
```
#### Run disponibles :
```makefile
run_all #qui permet de lancer tt les scripts
run_exo #lancer seulement les exos
```

## Cours

### Les Bases
pour afficher une valeur dans le terminal on fait :  
```py
# print variable
print(mavariable)
# or print value
print('string value')
```

<br>

pour faire une fonction :
```py
#declare fontiontion
def fontion(arg)->str:
    # do some things 
    return arg

#call fonction
fonction('value')
```

<br>

pour definir une variable global :
```py
global my_global_variable
```

<br>

pour faire une boucle :

```py
array = ['pomme','melon','fraise']
for fruit in array
    # do some things

# execution après la boucle
while True:
    # make some test 
    break

# execution après la boucle
```
<br>


pour faire une condition :
```py
if (False):
    #do some thing
elif (False):
    # do an other things
else:
    # comportement par défaut
```

<br>

pour faire un import :
```py
from enum import Enum
```
